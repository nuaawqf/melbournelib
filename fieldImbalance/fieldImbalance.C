/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Author
    Rick Morgans, All rights reserved

\*---------------------------------------------------------------------------*/

#include "fieldImbalance.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "surfaceFields.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(fieldImbalance, 0);

    addToRunTimeSelectionTable
    (
        functionObject,
        fieldImbalance,
        dictionary
    );
}


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::fieldImbalance::fieldImbalance
(
    const word& name,
    const Time& t,
    const dictionary& dict
)
:
    functionObject(name),
    time_(t),
    regionName_(polyMesh::defaultRegion),
    inletPatchName_(),
    outletPatchName_(),
    fieldName_(),
    isActive_(true)
{
    read(dict);
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

bool Foam::fieldImbalance::start()
{
    return true;
}


bool Foam::fieldImbalance::execute()
{
    if (!isActive_)
    {
      return false;
    }

    const fvMesh& mesh =
        time_.lookupObject<fvMesh>(regionName_);

    label inletPatchID=mesh.boundaryMesh().findPatchID(inletPatchName_);
    if (debug)
      {
	Info << "inletPatchID " << inletPatchID << endl;
      }

    if (inletPatchID < 0)
    {
      Info << "Inlet patch " << inletPatchName_ << "not found. Deactivating" << endl;
      isActive_=false;
      return false;
    }

    label outletPatchID=mesh.boundaryMesh().findPatchID(outletPatchName_);

    if (debug)
      {
	Info << "outletPatchID " << outletPatchID << endl;
      }

    if (outletPatchID < 0)
    {
      Info << "Outlet patch " << outletPatchName_ << "not found. Deactivating" << endl;
      isActive_=false;
      return false;
    }

    if (mesh.foundObject<volScalarField>(fieldName_))
    {
        const volScalarField& f = mesh.lookupObject<volScalarField>
        (
            fieldName_
        );

	scalar inletArea = gSum(mesh.magSf().boundaryField()[inletPatchID]);
	scalar inletSum =
	  gSum
	  (
	   f.boundaryField()[inletPatchID]*
	   	   mesh.magSf().boundaryField()[inletPatchID]
	   );

	if (debug)
	  {
	    Info << "inletArea :" << inletArea << endl;
	    Info << "inletSum :" << inletSum << endl;
	  }

	scalar inletAverage;

	if (inletArea > SMALL)
	{
	  inletAverage= inletSum/inletArea;
	}
	else
	{
	  inletAverage=0;
	}

	scalar outletArea = gSum(mesh.magSf().boundaryField()[outletPatchID]);
	scalar outletSum =
	  gSum
	  (
	   f.boundaryField()[outletPatchID]*
	   	   mesh.magSf().boundaryField()[outletPatchID]
	   );

	if (debug)
	  {
	    Info << "outletArea :" << outletArea << endl;
	    Info << "outletSum :" << outletSum << endl;
	  }

	scalar outletAverage;

	if (outletArea > SMALL)
	{
	  outletAverage= outletSum/outletArea;
	}
	else
	{
	  outletAverage=0;
	}

        Info << "fieldImbalance " << fieldName_
             << " inlet = " << inletAverage
             << " outlet = " << outletAverage
	     << " imbalance = " << outletAverage-inletAverage
	     << endl;

        return true;
    }
    /*
    else if (mesh.foundObject<volVectorField>(fieldName_))
    {
        const volVectorField& f = mesh.lookupObject<volVectorField>(fieldName_);

        volScalarField magF = mag(f);
      
        Info<< "Field " << fieldName_ << " magnitude min = "
            << Foam::min(magF).value()
            << " max = " << Foam::max(magF).value() << endl;
      Info << "Field " << fieldName_ << "is a volVectorField, currently not supported." << endl;

        return false;
    }
    */
    else
    {
        Info<< "Field "  << fieldName_ << " not found or not a volScalarField.  Skipping." << endl;

        return false;
    }
}


bool Foam::fieldImbalance::read(const dictionary& dict)
{

    isActive_=true;

    if (dict.found("region"))
    {
        dict.lookup("region") >> regionName_;
    }

    if (dict.found("name"))
    {
        dict.lookup("name") >> fieldName_;
    }
    else
    {
        isActive_=false;
    }

    if (dict.found("inlet"))
    {
        dict.lookup("inlet") >> inletPatchName_;
    }
    else
    {
        isActive_=false;
    }

    if (dict.found("outlet"))
    {
        dict.lookup("outlet") >> outletPatchName_;
    }
    else
    {
        isActive_=false;
    }

    if (isActive_)
    {
      Info<< "Creating fieldImbalance for field "
	  << fieldName_ 
	  << " between inlet patch "
	  << inletPatchName_
	  << " and outlet patch "
	  << outletPatchName_ << endl;
      return true;
    }
    else
    {
      Info<< "fieldImbalance inactive,  "
	  << " field: "
	  << fieldName_ 
	  << " inlet: "
	  << inletPatchName_
	  << " outlet: "
	  << outletPatchName_ << endl;
      return false;
    }
}

// ************************************************************************* //
