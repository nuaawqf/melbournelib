/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::waveFixedValueFvPatchField

Description
    Foam::waveFixedValueFvPatchField

SourceFiles
    waveFixedValueFvPatchField.C

\*---------------------------------------------------------------------------*/

#ifndef waveFixedValueFvPatchField_H
#define waveFixedValueFvPatchField_H

#include "Random.H"
#include "fixedValueFvPatchFields.H"
#include "fvMesh.H"
#include "volFields.H"
#include "surfaceFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                     Class waveFixedValueFvPatch Declaration
\*---------------------------------------------------------------------------*/

template<class Type>
class waveFixedValueFvPatchField
:
    public fixedValueFvPatchField<Type>
{
    // Private data

        //- Reference value
        Field<Type> refValueLow_;
        Field<Type> refValueHigh_;

        //- Amplitude
        scalar amplitude_;

        //- Frequency
        scalar frequency_;

        //- Reference Height
        scalar refHeight_;

        //- Current time index
        label curTimeIndex_;

    // Private member functions

        //- Return current scale
        scalar currentScale() const;
        Field<Type> currentScaleTwo() const;

public:

    //- Runtime type information
    TypeName("waveFixedValue");


    // Constructors

        //- Construct from patch and internal field
        waveFixedValueFvPatchField
        (
            const fvPatch&,
            const DimensionedField<Type, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        waveFixedValueFvPatchField
        (
            const fvPatch&,
            const DimensionedField<Type, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given waveFixedValueFvPatchField
        //  onto a new patch
        waveFixedValueFvPatchField
        (
            const waveFixedValueFvPatchField<Type>&,
            const fvPatch&,
            const DimensionedField<Type, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Construct as copy
        waveFixedValueFvPatchField
        (
            const waveFixedValueFvPatchField<Type>&
        );

        //- Construct and return a clone
        virtual tmp<fvPatchField<Type> > clone() const
        {
            return tmp<fvPatchField<Type> >
            (
                new waveFixedValueFvPatchField<Type>(*this)
            );
        }

        //- Construct as copy setting internal field reference
        waveFixedValueFvPatchField
        (
            const waveFixedValueFvPatchField<Type>&,
            const DimensionedField<Type, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchField<Type> > clone
        (
            const DimensionedField<Type, volMesh>& iF
        ) const
        {
            return tmp<fvPatchField<Type> >
            (
                new waveFixedValueFvPatchField<Type>(*this, iF)
            );
        }


    // Member functions

        // Access

            //- Return the ref value below wave surface
            const Field<Type>& refValueLow() const
            {
                return refValueLow_;
            }

            //- Return reference to the ref value below wave surface to allow adjustment
            Field<Type>& refValueLow()
            {
                return refValueLow_;
            }
    
            //- Return the ref value above wave surface
            const Field<Type>& refValueHigh() const
            {
                return refValueHigh_;
            }

            //- Return reference to the ref value above wave surface to allow adjustment
            Field<Type>& refValueHigh()
            {
                return refValueHigh_;
        
            }

            //- Return amplitude
            scalar amplitude() const
            {
                return amplitude_;
            }

            scalar& amplitude()
            {
                return amplitude_;
            }

            //- Return frequency
            scalar frequency() const
            {
                return frequency_;
            }

            scalar& frequency()
            {
                return frequency_;
            }

            //-  Return average wave height 
            scalar refHeight() const
            {
                return refHeight_;
            }

            //-  Return average wave height 
            scalar& refHeight()
            {
                return refHeight_;
            }


        // Mapping functions

            //- Map (and resize as needed) from self given a mapping object
            virtual void autoMap
            (
                const fvPatchFieldMapper&
            );

            //- Reverse map the given fvPatchField onto this fvPatchField
            virtual void rmap
            (
                const fvPatchField<Type>&,
                const labelList&
            );


        // Evaluation functions

            //- Update the coefficients associated with the patch field
            virtual void updateCoeffs();


        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#ifdef NoRepository
#   include "waveFixedValueFvPatchField.C"
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
